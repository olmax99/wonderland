(ns wonderland.core)

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))

(println "Cleanliness is the next to godliness")

(defn train
  []
  (map inc [2 6]) 
  (map inc [1 2 3 4]))


(map inc (train))

(map #(+ 3 %) (train))
     

